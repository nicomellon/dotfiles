# Environment variables set everywhere
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="safari"

# Homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"
