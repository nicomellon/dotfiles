 ## Installation
1. Make a directory to hold the bare repository:
```mkdir .dotfiles```
2. Clone the repository:
```git clone --bare <repo_url> .dotfiles```
3. Create an alias for easy git commands:
```alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'```
4. Checkout to update the fs:
```config checkout```
