local fn = vim.fn

-- automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
  	"git",
  	"clone",
  	"--depth",
  	"1",
  	"https://github.com/wbthomason/packer.nvim",
  	install_path,
  })
  print("Installing packer close and reopen Neovim...")
  vim.cmd([[packadd packer.nvim]])
end

-- autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

return packer.startup(function()
  -- general plugins
  use 'wbthomason/packer.nvim' -- have packer manage itself
  use 'nvim-lua/popup.nvim' -- an implementation of the Popup API from vim in Neovim
  use 'nvim-lua/plenary.nvim' -- useful lua functions used by lots of plugins
  use 'kyazdani42/nvim-web-devicons'

  -- status bar
  use 'nvim-lualine/lualine.nvim'

  -- colorschemes
  use {'catppuccin/nvim', as = 'catppuccin'}

  -- cmp plugins
  use 'hrsh7th/nvim-cmp' -- the completion plugin
  use 'hrsh7th/cmp-buffer' -- buffer completions
  use 'hrsh7th/cmp-path' -- path completions
  use 'saadparwaiz1/cmp_luasnip' -- snippet completions
  use 'hrsh7th/cmp-nvim-lsp'

  -- snippets
  use 'L3MON4D3/LuaSnip' --snippet engine
  use 'rafamadriz/friendly-snippets' -- a bunch of snippets to use

  -- LSP
  use 'neovim/nvim-lspconfig' -- enable LSP
  use 'williamboman/nvim-lsp-installer' -- simple to use language server installer

  -- Telescope
  use 'nvim-telescope/telescope.nvim'

  -- Treesitter
  use 'nvim-treesitter/nvim-treesitter'

  -- Git
-- 	use({ "lewis6991/gitsigns.nvim", commit = "c18e016864c92ecf9775abea1baaa161c28082c3" })

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
      packer.sync()
  end
end)
