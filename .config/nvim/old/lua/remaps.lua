--[[ helpers ]] --
-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { noremap = true, silent = true }

--[[ globals ]] --
-- set space as leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- [[ normal mode ]] --
-- begining & end of line in normal mode
keymap('n', 'H', '^', opts)
keymap('n', 'L', 'g_', opts)

-- telescope
keymap('n', '<leader>ff', '<cmd>Telescope find_files<cr>', opts)
keymap('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', opts)
keymap('n', '<leader>fb', '<cmd>Telescope buffers<cr>', opts)
keymap('n', '<leader>fh', '<cmd>Telescope help_tags<cr>', opts)

-- [[ visual ]] --
-- stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

