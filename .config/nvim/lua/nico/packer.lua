return require('packer').startup(function(use)

    use 'wbthomason/packer.nvim'  -- Packer can manage itself

    -- colorschemes
    use {'catppuccin/nvim', as = 'catppuccin'}

end)
