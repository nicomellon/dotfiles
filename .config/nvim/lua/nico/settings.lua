-- basic settings
vim.o.encoding = "utf-8"
vim.o.backspace = "indent,eol,start" -- backspace works on every char in insert mode
vim.o.exrc = true -- load config from .nvimrc or .exrc in the working directory
vim.o.hidden = true
vim.o.timeoutlen=1000
vim.o.ttimeoutlen=5
vim.o.wrap = false

-- sidebar
vim.o.signcolumn = 'yes'
vim.o.number = true
vim.o.relativenumber = true

-- indentation
vim.o.autoindent = true
vim.o.smartindent = true
vim.o.tabstop = 4 -- 1 tab = 4 spaces
vim.o.softtabstop = 4
vim.o.shiftwidth = 4 -- indentation rule
vim.o.expandtab = true -- expand tab to spaces

-- search
vim.o.incsearch = true -- starts searching as soon as typing, without enter needed
vim.o.ignorecase = true -- ignore letter case when searching
vim.o.smartcase = true -- case insentive unless capitals used in search
vim.o.hlsearch = false

vim.g.mapleader = " "
